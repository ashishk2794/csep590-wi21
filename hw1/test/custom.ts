import {expect} from "chai";
import {EngExp, EngExpError} from "../src/engexp";

describe("Custom test suite", () => {

    // Tests are created with the it() function, which takes a description
    // and the test body, wrapped up in a function.
    it("should throw EngExpErrors for mismatched capture groups", () => {

        // put the body of the test here
	function funcone() {
	   const e = new EngExp()
		.beginCapture().then(
	     	    new EngExp()
		       .beginCapture()
		       .match("a")
                       .endCapture()
                    .endCapture())
                    .asRegExp();
	}
	
        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
        expect(funcone).to.throw(EngExpError);
    });

    // You should modify the above test so it's interesting, and provide
    // at least 4 more nontrivial tests.
    it("should throw EngExpErrors for incorrect nestings", () => {
	
	function functwo() {
	   const e = new EngExp()
			.startOfLine()
			.beginCapture()
			.beginLevel()
			.digit().repeated(1,2)
			.endCapture()
			.endOfLine();
	}
        
	// Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.

        expect(functwo).to.throw(EngExpError);
    });

    it("should match nested levels deeper than 1", () => {
	
	const e1 = new EngExp().match("a").then(new EngExp().match("b").then(new EngExp().match("c").or("d")).repeated(2,3).then("e")).asRegExp();
	const e2 = new EngExp()
	  .match("a")
	    .beginLevel()
	    .match("b")
              .beginLevel()
              .match("c")
              .or("d")
              .endLevel()
            .repeated(2,3)
            .then("e")
            .endLevel()
          .asRegExp();
 
        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
	expect(e2.test("abcbcbce")).to.be.true;
	expect(e1).to.deep.equal(e2);
    });
    
    it("should mix nestings and levels that are properly formed", () => {
	
        const e = new EngExp()
	  .match("a")
            .beginLevel()
            .match("f")
              .beginLevel()
              .beginCapture()
              .match("b")
              .or("d")
              .endCapture() 
              .beginCapture()
              .match("c")
              .or("e")
                .beginLevel()
                .match("ig")
                .repeated(2,3)
                .endLevel()	
              .endCapture()
             .repeated(2,3)
             .endLevel()
	    .endLevel()
            .asRegExp();
        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.

	expect(e.test("afbeigigbeigig")).to.be.true;
        var result = e.exec("afbeigigbeigig");
	expect(e.test("afbeigbeig")).to.be.false;
    });
    
  it("levels and nestings match same expressions", () => {
	const e1 = new EngExp()
           .beginCapture()
           .match("a")
           .endCapture()
           .beginCapture()
             .beginCapture()
             .match("cde")
             .endCapture()
             .beginCapture()
             .match("fgh")
             .endCapture()
           .endCapture()
           .asRegExp();
    
        const e2 = new EngExp()
           .beginLevel()
           .match("a")
           .endLevel()
           .beginLevel()
              .beginLevel()
              .match("cde")
              .endLevel()
              .beginLevel()
              .match("fgh")
              .endLevel()
           .endLevel()
           .asRegExp();
 
        // Chai allows you to specify your test conditions with this nice
        // fluent syntax - look at the provided tests in test/engexp.ts for
        // for mor examples.
	expect(e1.test("acdefgh")).to.be.true;
	expect(e2.test("acdefgh")).to.be.true;

    });
});
