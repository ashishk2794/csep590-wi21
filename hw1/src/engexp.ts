export class EngExpError extends Error {}

export class EngExp {
    private flags: string = "m";
    private pattern: string = "";
    private call_chain_pattern_dict: { [key: number]: string} = {};
    private capture_level_dict: { [key: number]: number} = {};
    private capture_level: number = 0;
    private chain_level: number = 0;
    // You can add new fields here


    // Don't change sanitize()
    private static sanitize(s: string | EngExp): string | EngExp {
        if (s instanceof EngExp)
            return s;
        else {
            return s.replace(/[^A-Za-z0-9_]/g, "\\$&");
        }
    }

    // Don't change the following three public methods either
    public valueOf(): string {
        return this.asRegExp().source;
    }

    public toString(): string {
        return this.asRegExp().source;
    }

    public withFlags(flags: string) {
        if (/[^gimuy]/g.test(flags))
            throw new EngExpError("invalid flags");
        this.flags = "".concat(...new Set(flags));
        return this;
    }
    // End methods you shouldn't touch

    // asRegExp() will always be called before using your EngExp as a RegExp,
    // or before converting it into a string (for example to include via ``
    // template literals in another EngExp). You might want to take advantage
    // of this for debugging or error checking.
    public asRegExp(): RegExp {
        if (this.capture_level != 0 || this.chain_level != 0) {
	   throw new EngExpError("incorrect chaining and/or capturing");
        }
        return new RegExp(this.pattern, this.flags);
    }

    // There is a bug somewhere in this code... you'll need to find it

    public match(literal: string): EngExp {
        return this.then(literal);
    }

    public then(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})`;
        return this;
    }

    public startOfLine(): EngExp {
        this.pattern += "^";
        return this;
    }

    public endOfLine(): EngExp {
        this.pattern += "$";
        return this;
    }

    public zeroOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.zeroOrMore());
        else {
            this.pattern = `(?:${this.pattern})*`;
            return this;
        }
    }

    public oneOrMore(pattern?: EngExp): EngExp {
        if (pattern)
            return this.then(pattern.oneOrMore());
        else {
            this.pattern = `(?:${this.pattern})+`;
            return this;
        }
    }

    public optional(): EngExp {
        this.pattern = `(?:${this.pattern})?`;
        return this;
    }

    public maybe(pattern: string | EngExp): EngExp {
        this.pattern += `(?:${EngExp.sanitize(pattern)})?`;
        return this;
    }

    public anythingBut(characters: string): EngExp {
        this.pattern += `[^${EngExp.sanitize(characters)}]*`;
        return this;
    }

    public digit(): EngExp {
        this.pattern += "\\d";
        return this;
    }

    public repeated(min: number, max?: number): EngExp {
        if (max === undefined) {
            this.pattern = `(?:${this.pattern}){${min}}`;
        }
        else {
            this.pattern = `(?:${this.pattern}){${min},${max}}`;
        }
        return this;
    }

    public multiple(pattern: string | EngExp, min: number, max?: number) {
        if (max === undefined) {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${min}}`;
        }
        else {
            this.pattern += `(?:${EngExp.sanitize(pattern)}){${min},${max}}`;
        }
        return this;
    }

    // You need to implement these five operators:

    public or(pattern: string | EngExp): EngExp {
        // YOUR CODE HERE
	this.pattern = `(?:${this.pattern}|${EngExp.sanitize(pattern)})`;
	return this;
    }

    public beginCapture(): EngExp {
        // YOUR CODE HERE
	this.pattern += `(?:(`;
	this.capture_level = this.capture_level + 1;
	return this;
    }

    public endCapture(): EngExp {
        // YOUR CODE HERE
        if (this.capture_level == 0) {
	   throw new EngExpError("cannot close capture group without starting");
	}
	this.capture_level = this.capture_level - 1;		
	this.pattern += `))`;
	return this;
    }

    public beginLevel(): EngExp {
        // YOUR CODE HERE
        // console.log(this.pattern);
	this.call_chain_pattern_dict[this.chain_level] = this.pattern;
        this.capture_level_dict[this.chain_level] = this.capture_level;
 	this.chain_level = this.chain_level+1;
        this.capture_level = 0;
	this.pattern = "";
	return this;
    }

    public endLevel(): EngExp {
        // YOUR CODE HERE
        if (this.capture_level != 0) {
	   throw new EngExpError("cannot close level without closing groups");
        }
	if (this.chain_level == 0) {
	   throw new EngExpError("cannot close level without starting a level");	} 
        this.call_chain_pattern_dict[this.chain_level-1] += `(?:${this.pattern})`;
	this.chain_level = this.chain_level - 1;
	this.capture_level = this.capture_level_dict[this.chain_level];
        this.pattern = this.call_chain_pattern_dict[this.chain_level];
	return this;
    }
}

// ----- EXTRA CREDIT: named capture groups -----

// You'll need to modify a few parts of your EngExp implementation.
// First, change the signatures of beginCapture() and endCapture() to the following:
//
//     beginCapture(name?: string): EngExp
//     endCapture(name?: string): EngExp
//
// JavaScript regular expressions don't support named groups out of the box,
// so we'll need to extend them. Change the signature of asRegExp() to the following:
//
//     asRegExp(): NamedRegExp
//
// The interface for NamedRegExp is provided below (don't change it!) and depends on
// the new interface for NamedRegExpExecArray.

interface NamedRegExpExecArray extends RegExpExecArray {
    groups: Map<string, string | undefined>;
}

interface NamedRegExp extends RegExp {
    exec(s: string): NamedRegExpExecArray | null;
}

// Essentailly, a NamedRegExp is like a RegExp, but when you call its exec() method
// you get back an array with an extra field, groups, which is a hashmap from the
// names of capture groups to what those groups matched. You can access it like this:
//
//     let r = new EngExp().beginCapture("justf").then("f").endCapture().then("oo").asRegExp()
//     r.exec("foo").groups["justf"]  ==  "f"
//
// See the tests at the end of test/engexp.ts for more usage examples.
